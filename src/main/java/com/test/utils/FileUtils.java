package com.test.utils;

import com.test.model.ArticleSDO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileUtils {
    private static Logger logger = LogManager.getLogger(FileUtils.class);

    public static void writeToFile(String fileName, List<ArticleSDO> articleSDOs) {

        FileWriter writer;
        try {
            writer = new FileWriter(fileName);
            articleSDOs.forEach(a -> {
                try {
                    String temp = "- Title: " + a.getTitle() + " (link: " + a.getUrl() + ")\n";
                    //save to file
                    writer.write(temp);
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            });
            writer.close();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
