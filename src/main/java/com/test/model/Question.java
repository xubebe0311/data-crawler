package com.test.model;

import javax.persistence.*;

@Entity
@Table(name = "question")
public class Question {

    @Id
    private String id;

    private String url;

    private String title;

    @OneToOne()
    @JoinColumn(name = "username")
    private User author;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Question() {
    }

    public Question(String id, String url, String title, User author) {
        this.id = id;
        this.url = url;
        this.title = title;
        this.author = author;
    }
}
