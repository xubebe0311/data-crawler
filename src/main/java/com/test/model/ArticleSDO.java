package com.test.model;

public class ArticleSDO {

    private String id;

    private String url;

    private String title;

    private User author;

    public ArticleSDO() {
    }

    public ArticleSDO(String id, String url, String title, User author) {
        this.id = id;
        this.url = url;
        this.title = title;
        this.author = author;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }
}
