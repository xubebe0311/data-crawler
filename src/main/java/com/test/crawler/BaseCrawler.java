package com.test.crawler;

import com.test.model.ArticleSDO;
import com.test.model.User;
import com.test.repository.UserRepository;
import com.test.utils.SSLUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Service
abstract class BaseCrawler {
    private static Logger logger = LogManager.getLogger(BaseCrawler.class);

    private static final String attributeKey = "abs:href";

    private static final String userCssQuery = "a[href^=\"/u\"]";

    private static final String fullnameCssQuery = "a[href][class]";

    private static Map<String, ArticleSDO> articleSDOMap = new HashMap<>();

    @Autowired
    UserRepository userRepository;

    private final String JAVA_FILTER_REGEX = "^.*?(?i)(?<![\\w\\d])(Java|java|JAVA)(?![\\w\\d]).*$";

    void getPageLinks(HashSet<String> links, String URL, String cssPageQuery) {
        if (!links.contains(URL)) {
            try {
                if (links.add(URL)) {
                    logger.debug("Adding url : {}", URL);
                }
                SSLUtils.enableSSLSocket();
                Document document = Jsoup.connect(URL).ignoreHttpErrors(true).validateTLSCertificates(true).followRedirects(true).get();
                Elements otherLinks = document.select(cssPageQuery);

                // from each page we will get more pages and try to add to the page list
                for (Element page : otherLinks) {
                    getPageLinks(links, page.attr(attributeKey), cssPageQuery);
                }
            } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
                logger.error(e.getMessage());
            }
        }
    }

    //Connect to each link saved in the article and find all the articles in the page
    List<ArticleSDO> getArticles(HashSet<String> links, String cssItemQuery, String cssTitleQuery) {
        links.forEach(x -> {
            Document document;
            try {
                SSLUtils.enableSSLSocket();
                document = Jsoup.connect(x).ignoreHttpErrors(true).validateTLSCertificates(true).followRedirects(true).get();

                Elements postFeedItems = document.select(cssItemQuery);
                postFeedItems.forEach(postFeedItem -> {
                    List<Element> childrenElements = postFeedItem.children();
                    childrenElements.forEach(child -> {
                        User author = getUserFromElement(child);
                        getArticleFromElement(child, cssTitleQuery, author);
                    });
                });

            } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
                logger.error(e.getMessage());
            }
        });

        return new ArrayList<>(articleSDOMap.values());
    }

    private User getUserFromElement(Element child) {
        Elements userElement = child.select(userCssQuery);
        String userUrl = userElement.attr(attributeKey);
        String fullName = userElement.select(fullnameCssQuery).text();
        String username = null;
        // extract username from url
        List<String> userUrlTokens = Arrays.asList(StringUtils.tokenizeToStringArray(userUrl, "/"));
        if (!CollectionUtils.isEmpty(userUrlTokens)) {
            username = userUrlTokens.get(userUrlTokens.size() - 1);
        }
        User user = new User();
        user.setUsername(username);
        user.setName(fullName);
        user.setUrl(userUrl);
        if (!StringUtils.isEmpty(user.getUsername()) && Objects.isNull(userRepository.findOne(user.getUsername()))) {
            userRepository.save(user);
        }
        return user;
    }

    private void getArticleFromElement(Element child, String cssTitleQuery, User author) {
        Elements postArticles = child.select(cssTitleQuery);
        for (Element article : postArticles) {

            //Only retrieve the titles of the articles that contain Java
            if (article.text().matches(JAVA_FILTER_REGEX)) {
                String url = article.attr(attributeKey);
                if (StringUtils.isEmpty(url)) {
                    url = article.parentNode().attr(attributeKey);
                }
                ArticleSDO articleSDO = new ArticleSDO();
                articleSDO.setUrl(url);
                articleSDO.setTitle(article.text());
                articleSDO.setAuthor(author);

                // extract id from url
                List<String> list = Arrays.asList(StringUtils.tokenizeToStringArray(url, "-"));
                if (!CollectionUtils.isEmpty(list)) {
                    articleSDO.setId(list.get(list.size() - 1));
                }
                articleSDOMap.putIfAbsent(articleSDO.getId(), articleSDO);
            }
        }
    }

}
