package com.test.crawler;

import com.test.model.ArticleSDO;
import com.test.model.Question;
import com.test.repository.QuestionRepository;
import com.test.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class QuestionsCrawler extends BaseCrawler {

    private static HashSet<String> links = new HashSet<>();
    private static List<ArticleSDO> articles = new ArrayList<>();
    private static final String cssPageQuestionsQuery = "a[href^=\"/questions?page=\"]";
    private static final String cssQuestionItemQuery = "div[class^=\"question-item\"]";
    private static final String cssQuestionTitlesQuery = "h3";

    private static QuestionRepository questionRepository;

    @Autowired
    public QuestionsCrawler(QuestionRepository questionRepository) {
        QuestionsCrawler.questionRepository = questionRepository;
    }

    public void getQuestions() {
        getPageLinks(links, "https://viblo.asia/questions", cssPageQuestionsQuery);
        articles = getArticles(links, cssQuestionItemQuery, cssQuestionTitlesQuery);
        FileUtils.writeToFile("Java Questions", articles);
        List<Question> questions = articles.stream()
                .map(articleSDO -> new Question(articleSDO.getId(), articleSDO.getUrl(), articleSDO.getTitle(), articleSDO.getAuthor()))
                .collect(Collectors.toList());
        questions.forEach(post -> {
            if (Objects.isNull(questionRepository.findOne(post.getId()))) {
                questionRepository.save(post);
            }
        });
    }


}
