package com.test.crawler;

import com.test.model.ArticleSDO;
import com.test.model.Post;
import com.test.repository.PostRepository;
import com.test.utils.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Configurable
public class PostsCrawler extends BaseCrawler {
    private static HashSet<String> links = new HashSet<>();
    private static List<ArticleSDO> articles = new ArrayList<>();
    private static final String cssPageQuery = "a[href^=\"/?page=\"]";
    private static final String cssPostFeedItemQuery = "div[class^=\"post-feed-item\"]";
    private static final String cssPostTitlesQuery = "h3 a[href]";

    private final PostRepository postRepository;

    @Autowired
    public PostsCrawler(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public void getPosts() {
        getPageLinks(links, "https://viblo.asia", cssPageQuery);
        articles = getArticles(links, cssPostFeedItemQuery, cssPostTitlesQuery);
        FileUtils.writeToFile("Java Posts", articles);

        List<Post> posts = articles.stream()
                .map(articleSDO -> new Post(articleSDO.getId(), articleSDO.getUrl(), articleSDO.getTitle(), articleSDO.getAuthor()))
                .collect(Collectors.toList());
        posts.forEach(post -> {
            if (Objects.isNull(postRepository.findOne(post.getId()))) {
                postRepository.save(post);
            }
        });
    }
}
