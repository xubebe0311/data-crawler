package com.test;

import com.test.crawler.PostsCrawler;
import com.test.crawler.QuestionsCrawler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class Application {

    private final QuestionsCrawler questionsCrawler;

    private final PostsCrawler postsCrawler;

    @Autowired
    public Application(QuestionsCrawler questionsCrawler, PostsCrawler postsCrawler) {
        this.questionsCrawler = questionsCrawler;
        this.postsCrawler = postsCrawler;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        questionsCrawler.getQuestions();
        postsCrawler.getPosts();
    }
}
