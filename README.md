# Viblo crawler application
This is a java application to get post-feed-item and question-item from `https://viblo.asia/` and save to database

This java application is using Spring boot, Hibernate & MySql.

No unit test included, outputs have been saved as 2 file `Java Questions` & `Java Posts`
